import serial
import time
import csv

from datetime import datetime

ser = serial.Serial('/dev/ttyUSB0')
ser.flushInput()

entropyFile = 'entropy.dat'

pBit= 0

while True:
    #try:
        now = datetime.now()
        # dd/mm/YY H:M:S
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        ser_bytes = ser.readline()

        decoded_bytes = (ser_bytes[0:len(ser_bytes)-2].decode("utf-8"))
        print("{}, {}".format(dt_string, decoded_bytes))
        with open("nosalt_rads.csv","a") as f:
            writer = csv.writer(f,delimiter=",")
            writer.writerow([dt_string,decoded_bytes])

        countsPerSecond = "{}".format(decoded_bytes)
        countsPerSecond = countsPerSecond[5:6]
        print(countsPerSecond)

        if countsPerSecond:
            countsPerSecond = int(countsPerSecond)
            if (pBit != countsPerSecond):
                print("collate bits {}".format(countsPerSecond))
                pBit = countsPerSecond
                dt = datetime.now()
                print(type(dt.microsecond))
                s = str(dt.microsecond)
                writeBytes = s.encode()

                with open(entropyFile, "ab") as b:
                    b.write(writeBytes)
                    b.write("\n".encode())
                print(writeBytes)
   # except:
   #     print("Keyboard Interrupt")
   #     break
