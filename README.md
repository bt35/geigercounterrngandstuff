# What is it? 

This is a repo where I will track RNG stuff and Radio Active data collection using a [1]


## A test

Here is the background for 24 hours. 

![24 Hours Background](background-24-hours.png "24 Hours background")


Here is 24 hours of data when NoSalt [2] was present near the tube. 

![k40-NoSalt-24Hours](k40-NoSalt.png "24 Hours k40-NoSalt")



[1] https://mightyohm.com/blog/products/geiger-counter/
[2] https://en.wikipedia.org/wiki/Salt_substitute

Helpful References: 
- https://www.instructables.com/Portable-Raspberry-Pi-Geiger-Counter-With-Display/
